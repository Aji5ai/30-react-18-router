# React - Structure de projet et React Router

Utilise la documentation mettre en place une navigation entre des pages avec React Router.

## Ressources

- [https://gitlab.com/bastienapp/react18-router](https://gitlab.com/bastienapp/react18-router)
- [How To Structure React Projects From Beginner To Advanced](https://blog.webdevsimplified.com/2022-07/react-folder-structure/)
- [React Router - Getting started](https://reactrouter.com/en/main/start/tutorial)

## Contexte du projet

Une collègue a démarré une application, et n'a pas eu le temps de la finir. Il s'agit d'une application de vente de produits. Elle a réalisé le squelette de l'application, mais elle n'a pas eu l'occasion de créer la navigation entre les pages.

​Elle te donne le lien de son dépôt et te demande de le finir en son absence. Afin de t'aider, elle a commenté les endroits où tu devrais intervenir.

Commence par faire un fork du dépôt suivant : [https://gitlab.com/bastienapp/react18-router](https://gitlab.com/bastienapp/react18-router).

Ensuite clone ton fork en local.

### Structure d'un projet

Dans les briefs vu jusqu'à présent, tu as travaillé sur des applications "single page". La structure de dossier où tous tes composants sont dans un même dossier, ça passait encore.

Mais à partir du moment où tu vas travailler sur un site avec une navigation sur plusieurs pages, ça ne va plus le faire : certes des composants seront réutilisables, mais certains composants seront spécifiques à une page.

React est une bibliothèque, pas un framework : il n'impose pas de structure de dossier dans un projet.

Ce qui ouvre le débat sur quelle structure est appropriée pour quel projet. Je te conseille la lecture (complexe mais instructive) de la ressource suivante : [How To Structure React Projects From Beginner To Advanced](https://blog.webdevsimplified.com/2022-07/react-folder-structure/).

Pour des petits projets, je te propose cette structure, qui est facile à mettre en place :

- `/components` : contient les composant réutilisables, utiles à toute l'application
- `/pages` : parfois nommé "views", contient les différentes pages

Le dossier `pages` contiendra un dossier par page, qui lui même contiendra les composants spécifiques à cette page. Prenons l'exemple d'une page d'accueil qui présente une liste d'événements, on aurait la structure suivante :

- /page/Home/HomePage.jsx
- /page/Home/HomeEventList.jsx
- /page/Home/HomeEventItem.jsx

Et on pourrait imaginer des composants utilisés dans plusieurs pages :

- /page/components/StarRating.jsx
- /page/components/DatePicker.jsx
- /page/components/YoutubeContainer.jsx

### React Router

Maintenant que tu as une idée des structures possibles d'un projet React, tu vas pouvoir passer au vif du sujet : comment découper ton application en page, et créer des liens vers ces pages.

Pour cela, je te propose d'utiliser la bibliothèque **React Router**, et comme sa documentation officielle est très bien faite, pas besoin de t'en dire plus, à toi de la découvrir : [React Router - Getting started](https://reactrouter.com/en/main/start/tutorial).
​
## Modalités pédagogiques

- Un dépôt GitLab contient le code du projet
- Faire un fork du dépôt et cloner le fork en local
- Installer le module "React Router" dans le projet
- Modifier le composant "main.jsx" afin de créer des routes vers les pages "Home" (sur le chemin "/"), "ProductList" (sur le chemin "/products") et Cart (sur la route "/cart").
- Modifier le composant "Navigation" afin d'ajouter les liens vers les pages
- Les divers endroits à modifier sont indiqués par des commentaires dans le code
- Ajoute des commentaires afin d'expliquer ton code.

## Modalités d'évaluation

- Il est bien possible de naviguer entre les pages à l'aide de React Router
- Des commentaires expliquent le code
- Aucune erreur n'apparaît dans le terminal

## Livrables

- Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions
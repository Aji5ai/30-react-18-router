import "./Navigation.css";
import {Outlet, Link} from "react-router-dom";

function Navigation() {
  return (
    <ul className="Navigation">
      {/* TODO add links to the different pages */}

      {/* ANCIENNE VERSION qui recharge tout le document : */}
      {/* On ajoute les liens aux bons url pour que la navigation puisse nous y amener */}
      {/* <a href="/">
        <li>Home</li>
      </a> */}

      {/* VERSION AVEC LINK au lieu de <a> pour permettre de ne pas recharger toute la page mais juste le composant */}
      <li>
        <Link to={`/`}>Home</Link>
      </li>
      <li>
        <Link to={`/products`}>Products</Link>
      </li>
      <li>
        <Link to={`/cart`}>Cart</Link>
      </li>
    </ul>
  );
}

export default Navigation;

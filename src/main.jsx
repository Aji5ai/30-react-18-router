import React from "react";
import ReactDOM from "react-dom/client";
// TODO install and import react router
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";

const router = createBrowserRouter([
  /* On créé les chemins vers lequels rediriger l'utilisateur */
  {
    path: "/",
    element: (
      <Home />
    ) /* Par ex ici l'url http://localhost:5173/ affiche la page d'accueil */,
  },
  {
    path: "/products",
    element: (
      <ProductList />
    ) /* Et ici ça affiche http://localhost:5173/products */,
  },
  {
    path: "/cart",
    element: <Cart />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* TODO use react router here */}
    <RouterProvider router={router} /> {/*  Au lieu de mettre toutes les pages de composants ici, on ne met que la redirection du router (redirige sur les pages créées plus haut). On pourra utiliser la navigation dans components/navigation.jsx pour ajouter des liens aux autres pages */}
  </React.StrictMode>
);
